export const Search = () => {
    return (
      <div className="flex items-center py-2 px-3 border border-current rounded-2xl ">
        <label htmlFor="search" className="bx bx-search"></label>
        <input
          type="search"
          className="mx-2 bg-transparent outline-none"
          id="search"
          placeholder="Search..."
        />
        <button className="bx bx-microphone"></button>
      </div>
    );
  };