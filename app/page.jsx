import { Ad } from "@/components/Ad";

export default function Home ()  {
  return (
     <>
        <section className="flex">
           <div className="flex-1 flex flex-col items-stretch gap-4">
              <div className="h-52 bg-secondary"></div>
              <div className="w">
                 <div className="bg-secondary pt-[50%] !h-0 relative " >
                 </div>
              421.875
              </div>
              <div className="h-screen"></div>
           </div>
           <aside className="max-w-80 pl-4 flex-1 flex flex-col gap-4">
              <Ad />
           </aside>
        </section>
     </>
  );
};